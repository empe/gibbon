// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  serverUrl: 'https://gitlab.com',

  oauthApplicationId: '345b40b30e7cca1ad2398728f24a2f0adaf2201682d6d80f4e5041f74c883e4c',
  oauthScopes: 'api read_user',

  gitlabGroup: 1808217,
  columns: ['critical', 'support', 'enhancement', 'discussion', 'suggestion']

};
