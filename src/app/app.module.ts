import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CanvasComponent } from './canvas/canvas.component';
import { GitlabModule } from './gitlab/gitlab.module';
import { RouterModule } from '@angular/router';
import { MapToIterablePipe } from './canvas/map-to-iterable.pipe';
import { IssueComponent } from './canvas/issue.component';
import { ColumnComponent } from './canvas/column.component';
import { LoginComponent } from './login.component';
import { AuthGuard } from './auth.guard';
import { OAuthModule } from 'angular-oauth2-oidc';
import { ErrorPageComponent } from './error-page.component';
import { routes } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CanvasComponent,
    MapToIterablePipe,
    IssueComponent,
    ColumnComponent,
    LoginComponent,
    ErrorPageComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    OAuthModule.forRoot(),
    BrowserModule,
    GitlabModule
  ],
  providers: [ AuthGuard ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
