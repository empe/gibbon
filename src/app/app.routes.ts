
import {Routes} from '@angular/router';
import {LoginComponent} from './login.component';
import {CanvasComponent} from './canvas/canvas.component';
import {AuthGuard} from './auth.guard';
import {UserResolve} from './gitlab/resolve/user-resolve.service';
import {GroupResolve} from './gitlab/resolve/group-resolve.service';
import {IssuesResolve} from './gitlab/resolve/issues-resolve';
import {ErrorPageComponent} from './error-page.component';

export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: CanvasComponent,
    canActivate: [ AuthGuard ],
    resolve: {
      user: UserResolve,
      group: GroupResolve,
      issues: IssuesResolve
    }
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: ErrorPageComponent,
  },

];
