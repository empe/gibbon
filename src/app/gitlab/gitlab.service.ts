import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import { Project } from './model/project';
import { User } from './model/user';
import { Group } from './model/group';
import 'rxjs/add/operator/toPromise';
import {Issue} from './model/issue';
import {OAuthService} from 'angular-oauth2-oidc';

@Injectable()
export class GitlabService {

  constructor(private oauthService: OAuthService, private http: Http) {}

  public getUser(): Promise<User> {
    return this.get(`/user`).toPromise()
      .then(response => response.json() as User);
  }

  public getGroup(id: number): Promise<Group> {
    return this.get(`/groups/${id}`).toPromise()
      .then(response => response.json() as Group);
  }

  public listGroups(): Promise<Group[]> {
    return this.get('/groups').toPromise()
      .then(response => response.json() as Group[]);
  }


  public listProjectsInGroup(groupId: String): Promise<Project[]> {
    return this.get(`/groups/${groupId}/projects`).toPromise()
      .then(response => response.json() as Project[]);
  }

  public listIssuesInGroup(groupId: number): Promise<Issue[]> {
    return this.get(`/groups/${groupId}/issues`).toPromise()
      .then(response => response.json() as Issue[]);
  }

  private get(endpoint) {

    const url = `${environment.serverUrl}/api/v4${endpoint}`;
    const headers = new Headers();
    headers.append('Authorization', 'Bearer ' + this.oauthService.getAccessToken());

    return this.http.get(url, {
      headers: headers
    });
  }

}
