import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { User } from '../model/user';
import { GitlabService } from '../gitlab.service';

@Injectable()
export class UserResolve implements Resolve<User> {

  constructor(private service: GitlabService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.service.getUser();
  }

}
