import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { GitlabService } from '../gitlab.service';
import { environment } from '../../../environments/environment';
import {Issue} from '../model/issue';

@Injectable()
export class IssuesResolve implements Resolve<Issue[]> {

  constructor(private service: GitlabService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.service.listIssuesInGroup(environment.gitlabGroup);
  }

}
