export class Milestone {
  constructor(
    public title: string,
    public description: string,
    public project_id: string,
    public id: number
  ) {}
}
