export class Project {
  constructor(
    public id: number,
    public name: string,
    public path: string,
    public web_url: string,
  ) {}
}
