export class Label {
  constructor(
    public id: number,
    public name: string,
    public username: string,
    public email: string,
    public avatar_url: string) {}
}
