import { Component } from '@angular/core';
import {OAuthService} from 'angular-oauth2-oidc';
import {environment} from '../environments/environment';

@Component({
  selector: 'gbn-root',
  template: `
    <div class="container">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">{{ title }}</a>
          </div>
          <div id="navbar">
            <!--<router-outlet name="navigation"></router-outlet>-->
          </div>
        </div>
      </nav>
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent {

  public title =  'GIBBON';

  constructor(private oauthService: OAuthService) {

    // OAUTH Configuration
    this.oauthService.loginUrl = `${environment.serverUrl}/oauth/authorize`;
    this.oauthService.redirectUri = window.location.href.replace('login', '');
    this.oauthService.clientId = environment.oauthApplicationId;
    this.oauthService.scope = environment.oauthScopes;
    this.oauthService.oidc = false;
    this.oauthService.logoutUrl = this.oauthService.redirectUri;

    this.oauthService.tryLogin({});
  }

}
