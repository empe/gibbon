import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Group } from '../gitlab/model/group';
import { Issue } from '../gitlab/model/issue';
import { environment } from '../../environments/environment';
import { Project } from '../gitlab/model/project';

@Component({
  selector: 'gbn-canvas',
  templateUrl: `./canvas.component.html`,
  styleUrls: ['./canvas.component.less']
})
export class CanvasComponent implements OnInit {

  group: Group;
  issues: Issue[];
  model: { [id: string]: Issue[] };
  private projects: { [id: string]: Project };
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.group = this.route.snapshot.data['group'];
    this.issues = this.route.snapshot.data['issues'];
    this.projects = this.flattenProjects(this.group);
    this.buildModel(this.issues, environment.columns);
  }

  private flattenProjects(group: Group): {[id: string]: Project} {
    const result: {[id: string]: Project} = {};
    group.projects.forEach(project => { result[`${project.id}`] = project; });

    return result;
  }

  private buildModel(issues: Issue[], labels: string[]): void {
      this.model = {};
      for (const label of labels){
        this.model[label] = [];
      }

      for (const issue of issues){
          for (const label of labels){
            if (issue.labels.includes(label)) {
              const eissue = this.enrichIssue(issue);
              this.model[label].push(eissue);
            }
          }
      }
  }


  private enrichIssue(issue: Issue): Issue {
    console.log(this.projects);
    issue.projectPath = this.projects[issue.project_id].name;
    return issue;
  }



}
