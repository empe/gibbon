import {Component, Input, OnInit} from '@angular/core';
import {Issue} from '../gitlab/model/issue';

@Component({
  selector: 'gbn-issue',
  template: `
    <li class='card user-can-drag' style='margin-top: 2px;'>
      <div class='card-header'>
        <h4 class='card-title'>
          <a [href]='issue?.web_url' target='_blank'>{{ issue.title }}</a>
        </h4>
        <div class='card-assignee' *ngIf='issue.assignee'>
          <a [href]='issue.assignee?.web_url' >
            <img [src]='issue.assignee?.avatar_url' class='avatar' width='40' height='40'>
          </a>
        </div>
      </div>
      <div class='card-footer'>
        <div>{{issue.projectPath}}#{{issue.iid}}</div>
      </div>
      <div>
        <span *ngFor='let label of issue.labels'
              class='label label-default' style='margin-right: 2px;'>
          {{label}}
        </span>
      </div>
    </li>`
})
export class IssueComponent implements OnInit {

  @Input() issue: Issue;

  constructor() { }

  ngOnInit() {
  }

}
