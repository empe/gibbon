import {Component, Input, OnInit} from '@angular/core';
import { Issue } from '../gitlab/model/issue';

@Component({
  selector: 'gbn-column',
  template: `
    <div class="board">
      <div class="board-inner">
        <header class="board-header has-border">
          <h3 class="board-title">{{ name }}</h3>
        </header>
        <div class="board-list-component">
          <ul class="board-list">
            <gbn-issue [issue]="issue" *ngFor="let issue of issues">
            </gbn-issue>
          </ul>
        </div>
      </div>
    </div>
  `
})
export class ColumnComponent implements OnInit {

  @Input() name: string;
  @Input() issues: Issue[];

  constructor() { }

  ngOnInit() {}

}
