# GIBBON - Group level issue board for GitLab

The project is designed to provide **group** level view of issues, useful for standups and team progress tracking.

## SETUP

1. Fork the repository.
2. Adjust configuration in `environment.ts` file.
3. Enable Gitlab Pages for the project. 

And voila!

## DEVELOPMENT

This project was generated with [Angular CLI](https://github.com/angular/angular-cli):
 * `ng serve` for a dev server. Navigate to `http://localhost:4200/`
 
## NOTICE 

* Icon made by [Roundicons](https://www.flaticon.com/authors/roundicons) from www.flaticon.com
