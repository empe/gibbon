import { GibonPage } from './app.po';

describe('gibon App', () => {
  let page: GibonPage;

  beforeEach(() => {
    page = new GibonPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
